class Controller {
    constructor(model, view) {
        this._model = model;
        this._view = view;

        this._view.pridejUdalost("pridat", () => this.pridatPolozku())
        this._view.pridejUdalost("odebrat", (index) => this.odebratPolozku(index))
        this._view.pridejUdalost("filtrovat", () => this.filtrovatPolozky());
    }

    pridatPolozku() {
        var nazev = document.getElementById('nazev').value;
        var vyrobce = document.getElementById('vyrobce').value;
        var nosnost =parseInt(document.getElementById('nosnost').value);
        var vyuziti = document.getElementById('vyuziti').value;
        var druh = document.getElementById('druh').value;
        var dosah = document.getElementById('dosah').value;
        var osy = document.getElementById('osy').value;
        var zatizeni = document.getElementById('zatizeni').value;

            this._model.pridejPolozku(nazev, vyrobce, nosnost, vyuziti, druh, dosah, osy, zatizeni);

    }

    odebratPolozku(index) {
        this._model.odebratPolozku(index);
    }

    filtrovatPolozky(){
        let vaha = document.getElementById('vaha').value;
        console.log(vaha);
        if(vaha) {
            this._model.filtrovatPolozky(parseInt(vaha));
        } else {
            this._model.dostatPolozky();
        }

    }
}

class View extends EventEmitter {
    constructor(model) {
        super("view");
        this._model = model;

        this._model.pridejUdalost('vypsat', () => this.vypisPolozky())
        document.getElementById("b-pridej").addEventListener('click', () => this.vykonejUdalost('pridat'));
        document.getElementById("b-filtr").addEventListener('click', () => this.vykonejUdalost('filtrovat'));
        this.vypisPolozky()
    }

    vypisPolozky() {
        let seznam = document.getElementById("jmena");
        let polozky = this._model.polozky;

        while (seznam.firstChild) {
            seznam.firstChild.remove();
        }

        polozky.forEach( (fn, index) => {
            let li = document.createElement("li");
            let div = document.createElement("div");
            let ul = document.createElement("ul");
            let nazev = document.createElement("h3");
            let druh = document.createElement("p");
            let vyrobce = document.createElement("li");
            let nosnost = document.createElement("li");
            let vyuziti = document.createElement("li");
            let dosah = document.createElement("li");
            let osy = document.createElement("li");
            let zatizeni = document.createElement("li");
            let tlacitka = document.createElement("div");
            //let tl_uprav = document.createElement("button");
            let tl_smaz = document.createElement("button");


            nazev.appendChild(document.createTextNode(fn.nazev))

            druh.appendChild(document.createTextNode(fn.druh));

            vyrobce.appendChild(document.createTextNode("Výrobce: " + fn.vyrobce));
            ul.appendChild(vyrobce);
            nosnost.appendChild(document.createTextNode("Nosnost: " + fn.nosnost + " kg"));
            ul.appendChild(nosnost);
            vyuziti.appendChild(document.createTextNode("Využití: "+ fn.vyuziti));
            ul.appendChild(vyuziti);
            dosah.appendChild(document.createTextNode("Dosah: "+ fn.dosah + " mm"));
            ul.appendChild(dosah);
            osy.appendChild(document.createTextNode("Počet os: "+ fn.osy));
            ul.appendChild(osy);
            zatizeni.appendChild(document.createTextNode("Zatížení: "+ fn.zatizeni  + " kg"));
            ul.appendChild(zatizeni);

            tl_smaz.innerText = "Smazat";
            //tl_uprav.innerText = "Upravit";


            div.appendChild(nazev);
            div.appendChild(druh);
            li.appendChild(div);
            li.appendChild(ul);
            //tlacitka.appendChild(tl_uprav);
            tlacitka.appendChild(tl_smaz);
            li.appendChild(tlacitka);

            tl_smaz.addEventListener('click', () => this.vykonejUdalost('odebrat', fn.id));
            //tl_uprav.addEventListener('click', () => this.vykonejUdalost('upravit', fn.id));
            seznam.appendChild(li);
            }
        )
    }
}

class Model extends EventEmitter {
    constructor() {
        super('model');
        this._polozky = Array();
        const robotyData = [];
        /* testovaci data
        const robotyData = [
            { nazev: "LR Mate 200iD", vyrobce: "FANUC", nosnost: 7, vyuziti: "Víceúčelový vysokokapacitní robot", druh: "Standard", dosah: 717, osy: 6, zatizeni: 1000 },
            { nazev: "M-710iC/50H", vyrobce: "FANUC", nosnost: 50, vyuziti: "Paletizační robor", druh: "Standard", dosah: 2003, osy: 5, zatizeni: 1500 },
            { nazev: "ARC Mate 100iC/12S", vyrobce: "FANUC", nosnost: 12, vyuziti: "Robot pro svařování elektrickým obloukem", druh: "Standard", dosah: 1098, osy: 6, zatizeni: 1200 },
            { nazev: "P-50iB/10L", vyrobce: "FANUC", nosnost: 10, vyuziti: "Lakovací robot", druh: "Standard", dosah: 1800, osy: 6, zatizeni: 800 },
            { nazev: "M-710iC/50T", vyrobce: "FANUC", nosnost: 50, vyuziti: "Víceúčelový robot s vrchní montáží", druh: "Standard", dosah: 1900, osy: 6, zatizeni: 1300 },
            { nazev: "M-2000iA/1200", vyrobce: "FANUC", nosnost: 1200, vyuziti: "Užitečné zatížení vysoko přes tunu", druh: "Standard", dosah: 3437, osy: 6, zatizeni: 2400 }
        ];
        */
        this.verze = 1;
        this.db = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        this.spojeni = null;

        var pozadavek = this.db.open("db-roboty", this.verze);
        pozadavek.onupgradeneeded = function (event) {
            window.console.log("upgrade DB");

            var zaznamyStore = pozadavek.result.createObjectStore("zaznamy", {keyPath: "id", autoIncrement : true});
            zaznamyStore.createIndex("indexNazev", "nazev", {unique: false});
            zaznamyStore.createIndex("indexVyrobce", "vyrobce", {unique: false});
            zaznamyStore.createIndex("indexVaha", "nosnost", {unique: false});
            for (let i in robotyData) {
                zaznamyStore.add(robotyData[i]);
            }
        }

        pozadavek.onsuccess = (request) => this.spojeniOk(request);
    }

    spojeniOk(event) {
        this.spojeni = event.target.result;
        window.console.log(event.target.result);
        this.dostatPolozky();
    }

    set polozky(pole) {
        this._polozky = pole;
    }

    get polozky() {
        return this._polozky;
    }

    dostatPolozky() {
        if(this.spojeni) {
            let objStore = this.spojeni.transaction("zaznamy", "readwrite").objectStore("zaznamy");
            let request = objStore.getAll();
            request.onsuccess = function (ev) {
                window.console.log("data: ", ev.target);
                this.polozky = ev.target.result;
                this.vykonejUdalost('vypsat');
            }.bind(this)
        }
    }

    pridejPolozku(nazev = "", vyrobce = "", nosnost = 0, vyuziti = "", druh = "", dosah = 0, osy = 0, zatizeni = 0) {
        let polozka = {nazev: nazev, vyrobce: vyrobce, nosnost: nosnost, vyuziti: vyuziti, druh: druh, dosah: dosah, osy: osy, zatizeni: zatizeni};

        if(this.spojeni) {
            let objStore = this.spojeni.transaction("zaznamy", "readwrite").objectStore("zaznamy");
            let request = objStore.add(polozka);
            request.onsuccess = function (ev) {
                window.console.log("zapis dat Ok", ev.target);
                this.vykonejUdalost('vypsat');
            }.bind(this)
        }
        this.dostatPolozky();
        window.console.log(this._polozky);
    }

    upravPolozku(id, nazev = "", vyrobce = "", nosnost = 0, vyuziti = "", druh = "", dosah = 0, osy = 0, zatizeni = 0) {
        let polozka = {nazev: nazev, vyrobce: vyrobce, nosnost: nosnost, vyuziti: vyuziti, druh: druh, dosah: dosah, osy: osy, zatizeni: zatizeni};

        if(this.spojeni) {
            let objStore = this.spojeni.transaction("zaznamy", "readwrite").objectStore("zaznamy");
            let request = objStore.put(polozka, id);
            request.onsuccess = function (ev) {
                window.console.log("zapis dat Ok", ev.target);
                this.vykonejUdalost('vypsat');
            }.bind(this)
        }
        this.dostatPolozky();
        window.console.log(this._polozky);
    }

    odebratPolozku(id) {
        if(this.spojeni) {
            let objStore = this.spojeni.transaction("zaznamy", "readwrite").objectStore("zaznamy");
            let request = objStore.delete(id);
            request.onsuccess = function (ev) {
                window.console.log("smazani dat Ok");
                alert("Proběhlo smazání položky s id: " + id);
                this.vykonejUdalost('vypsat');
            } .bind(this)
        }
        this.dostatPolozky();
    }
    filtrovatPolozky(filtr) {
        if(this.spojeni) {
            this.polozky = [];
            console.log("vyhledani dat Ok", filtr);
            let objStore = this.spojeni.transaction("zaznamy", "readwrite").objectStore("zaznamy");
            let index = objStore.index('indexVaha');
            let request = index.openCursor(IDBKeyRange.only(filtr));
            request.onsuccess = function (ev) {
                let cursor = ev.target.result;
                //window.console.log("vyhledani dat Ok", cursor);
                if(cursor) {
                    this._polozky.push(cursor.value);
                    console.log("zaznam: ", cursor.value);
                    cursor.continue();
                }
                this.vykonejUdalost('vypsat');
            } .bind(this)
        }
    }
}
