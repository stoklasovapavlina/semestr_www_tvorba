class IndexovanaDB {
    constructor() {
        this.verze = 1;
        this.db = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
        this.spojeni = null;

        var request = this.db.open("db-jmena", this.verze);
        request.onupgradeneeded = function (event) {
            window.console.log("upgrade DB");

            var zaznamyStore = request.result.createObjectStore("zaznamy", {keyPath: "id", autoIncrement : true});
            zaznamyStore.createIndex("indexJmeno", "jmeno", {unique: false});
            zaznamyStore.createIndex("indexPrijmeni", "prijmeni", {unique: false});
        }

        request.onsuccess = (request) => this.spojeniOk(request);
    }

    spojeniOk(event) {
        this.spojeni = event.target.result;
        window.console.log(event.target.result);
    }

    ulozJmeno(jmeno, prijmeni) {
        if(this.spojeni) {
            let objStore = this.spojeni.transaction("zaznamy", "readwrite").objectStore("zaznamy");
            let request = objStore.put({jmeno: jmeno, prijmeni: prijmeni});
            request.onsuccess = function (ev) {
                window.console.log("zapis dat Ok", ev.target);
                this.vypisJmeno();
            }.bind(this)
        }
    }

    vypisData() {
        if(this.spojeni) {
            let objStore = this.spojeni.transaction("zaznamy", "readwrite").objectStore("zaznamy");
            let request = objStore.getAll();
            request.onsuccess = function (ev) {
                window.console.log("data: ", ev.target);
                ev.target.result.forEach( (ev) => {
                    window.console.log("zaznam: ", ev.jmeno, ev.prijmeni);
                })
            }
        }
    }

    vypisData2() {
        if(this.spojeni) {
            let objStore = this.spojeni.transaction("zaznamy", "readwrite").objectStore("zaznamy");
            let request = objStore.openCursor();
            request.onsuccess = function (ev) {
                let cursor = ev.target.result;
                if(cursor) {
                    window.console.log("zaznam: ", cursor.key, cursor.value.jmeno, cursor.value.prijmeni);
                    cursor.continue();
                }
            }
        }
    }

    vypisJmeno() {
        if(this.spojeni) {
            let objStore = this.spojeni.transaction("zaznamy", "readwrite").objectStore("zaznamy");
            let index = objStore.index('indexJmeno');
            let request = index.openCursor();
                // index.get('Tomas');
            request.onsuccess = function (ev) {
                let cursor = ev.target.result;
                if(cursor) {
                    window.console.log("zaznam: ", cursor.key, cursor.value.jmeno, cursor.value.prijmeni);
                    cursor.continue();
                }
            }
        }
    }

    //zprovoznit vypis jmeno
    //vypsat data do seznamu nekam vedle
    //po kliknuti na zaznam v seznamu ho smaze z DB
}