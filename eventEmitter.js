class EventEmitter {
    constructor(cl) {
        this._trida = cl; //nemusi byt, slouzi pro debug
        this._udalosti = {};
    }

    pridejUdalost(nazev, navratovaFunkce) {
        if(!this._udalosti[nazev]) {
            this._udalosti[nazev] = [];
        }
        this._udalosti[nazev].push(navratovaFunkce);
        window.console.log(this._trida, "registrujUdalost: " + nazev, navratovaFunkce.toString());
    }

    vykonejUdalost(nazev, data) {
        var poleFunkci = this._udalosti[nazev];
        if(poleFunkci) {
            poleFunkci.forEach(fn => { //fn => {} je stejné jako function(fn) {} ale nemění se scope
                window.console.log(this._trida, "vykonejUdalost: " + nazev, fn.toString());
                //TODO proc je tam null?
                fn.call(null, data);
            })
        }
    }
}